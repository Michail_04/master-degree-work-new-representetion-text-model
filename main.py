import sys
import os
from PyQt5 import QtWidgets, QtCore
import disign
import test as testScript

class MainForm(QtWidgets.QMainWindow, disign.Ui_MainWindow):
	def __init__(self):
		super().__init__()
		self.setupUi(self)
		self.toggleShowbar(True)
		
		self.getDocumentBtn.clicked.connect(self.showOpenDialog)
		self.documents.itemClicked.connect(self.itemClick)
	
	def toggleShowbar(self, status):
		if status == True:
			self.progressBar.hide()
		else:	
			self.progressBar.show()
	
	def showOpenDialog(self):
		self.documents.clear()
		file = QtWidgets.QFileDialog.getOpenFileName(
			self,
			''
			"/home"
		)
		self.getDocumentBtn.setEnabled(False)
		docs = testScript.init(file[0]);
		for doc in docs:
			self.documents.addItem(doc)
		self.setWindowTitle('Документов: ' + str(len(docs)))
			

			
		self.getDocumentBtn.setEnabled(True)
		
	def itemClick(self, item):
		os.system('notepad.exe '+item.text())
	
		
def main():
	app = QtWidgets.QApplication(sys.argv)
	window = MainForm()
	window.show()
	app.exec_()

main()