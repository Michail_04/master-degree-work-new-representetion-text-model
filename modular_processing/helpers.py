#! python3

import sys
import collections
import hashlib
from gensim.utils import smart_open
from gensim.models.word2vec import LineSentence
from gensim.models.phrases import Phrases, Phraser

hashSize = 1000 * 1000 * 8

def getHash(data):
	return hashlib.md5(data.encode('utf-8')).hexdigest()

def df(words, markerDf):
	result = []
	tf_text = collections.Counter(words);
	for prop in tf_text: 
		if tf_text[prop] > markerDf:
			result.append(prop)
	if not len(result):
		return words
	else:
		return result
	
def deleteStopWords (wordsColections, stopWordsColection):
	return [word for word in wordsColections if word.split('_')[0] not in stopWordsColection]

def num_replace(word):
    newtoken = 'x' * len(word)
    nw = newtoken + '_NUM'
    return nw

def clean_token(token, misc):
    """
    :param token:
    :param misc:
    :return:
    """
    out_token = token.strip().replace(' ', '')
    if token == 'Файл' and 'SpaceAfter=No' in misc:
        return None
    return out_token

def clean_lemma(lemma, pos):
    """
    :param lemma:
    :param pos:
    :return:
    """
    out_lemma = lemma.strip().replace(' ', '').replace('_', '').lower()
    if '|' in out_lemma or out_lemma.endswith('.jpg') or out_lemma.endswith('.png'):
        return None
    if pos != 'PUNCT':
        if out_lemma.startswith('«') or out_lemma.startswith('»'):
            out_lemma = ''.join(out_lemma[1:])
        if out_lemma.endswith('«') or out_lemma.endswith('»'):
            out_lemma = ''.join(out_lemma[:-1])
        if out_lemma.endswith('!') or out_lemma.endswith('?') or out_lemma.endswith(',') \
                or out_lemma.endswith('.'):
            out_lemma = ''.join(out_lemma[:-1])
    return out_lemma
