import gensim
import numpy
import nltk
import math
from nltk.corpus import stopwords
from modular_processing.unify import unify_sym
from modular_processing.helpers import *
from ufal.udpipe import Model, Pipeline
from tinydb import TinyDB, Query, where

nltk.download('stopwords')
russionStopwords = stopwords.words('russian')

db = TinyDB('db.json')
documentQuery = Query();
wordQuery = Query();

documents = db.table('documents')

udpipeModelPath = 'udpipe.model'
modelPath = 'model.bin'

markerDf = 4; 
markerSemantic = 0.7; 
countSemantic = 10

model = Model.load(udpipeModelPath)

word2vecModel = gensim.models.KeyedVectors.load_word2vec_format(modelPath, binary=True)

def getCos(a,b):
	sum = 0;
	sumA = 0;
	sumB = 0;
	i = 0;
	while i < len(a):
		sum = sum + (a[i] * b[i])
		sumA = sumA + (a[i] * a[i])
		sumB = sumB + (b[i] * b[i])
		i = i + 1
	
	return round(1 - (sum / (math.sqrt(sumA) * math.sqrt(sumB))),4)
	
def getVector(words):
	vector = []
	for word in words:
		if not len(vector):
			vector = word2vecModel.get_vector(word)
		else :
			vector = vector + word2vecModel.get_vector(word)
	return vector;

def init(textPath):
	file = open(textPath,'r',encoding='utf-8')
	text = file.read()
	documentHash = getHash(text)
	document = documents.search(documentQuery.hash == documentHash)
	
	if not len(document):
		documents.insert({
			'hash': documentHash,
			'vector': '',
			'path': ''
		})
		document = documents.search(documentQuery.hash == documentHash)

		processPipeline = Pipeline(model, 'tokenize', Pipeline.DEFAULT, Pipeline.DEFAULT, 'conllu')

		def process(pipeline, text='Строка', keep_pos=True, keep_punct=False):
			entities = {'PROPN'}
			named = False
			memory = []
			mem_case = None
			mem_number = None
			tagged_propn = []
			
			# обрабатываем текст, получаем результат в формате conllu:
			processed = pipeline.process(text)

			# пропускаем строки со служебной информацией:
			content = [l for l in processed.split('\n') if not l.startswith('#')]

			# извлекаем из обработанного текста леммы, тэги и морфологические характеристики
			tagged = [w.split('\t') for w in content if w]
			
			for t in tagged:
				if len(t) != 10:
					continue
				(word_id, token, lemma, pos, xpos, feats, head, deprel, deps, misc) = t
				token = clean_token(token, misc)
				lemma = clean_lemma(lemma, pos)
				if not lemma or not token:
					continue
				if pos in entities:
					if '|' not in feats:
						tagged_propn.append('%s_%s' % (lemma, pos))
						continue
					morph = {el.split('=')[0]: el.split('=')[1] for el in feats.split('|')}
					if 'Case' not in morph or 'Number' not in morph:
						tagged_propn.append('%s_%s' % (lemma, pos))
						continue
					if not named:
						named = True
						mem_case = morph['Case']
						mem_number = morph['Number']
					if morph['Case'] == mem_case and morph['Number'] == mem_number:
						memory.append(lemma)
						if 'SpacesAfter=\\n' in misc or 'SpacesAfter=\s\\n' in misc:
							named = False
							past_lemma = '::'.join(memory)
							memory = []
							tagged_propn.append(past_lemma + '_PROPN')
					else:
						named = False
						past_lemma = '::'.join(memory)
						memory = []
						tagged_propn.append(past_lemma + '_PROPN')
						tagged_propn.append('%s_%s' % (lemma, pos))
				else:
					if not named:
						if pos == 'NUM' and token.isdigit():  # Заменяем числа на xxxxx той же длины
							lemma = num_replace(token)
						tagged_propn.append('%s_%s' % (lemma, pos))
					else:
						named = False
						past_lemma = '::'.join(memory)
						memory = []
						tagged_propn.append(past_lemma + '_PROPN')
						tagged_propn.append('%s_%s' % (lemma, pos))
			
			if not keep_punct:
				tagged_propn = [word for word in tagged_propn if word.split('_')[1] != 'PUNCT']
			tagged_propn = deleteStopWords(tagged_propn, russionStopwords)
			tagged_propn = df(tagged_propn, markerDf)
			return tagged_propn

		normalizeWordsList = process(processPipeline, text)
		
		result = []
		for word in normalizeWordsList:
			if word in word2vecModel:
				result.append(word)
				for i in word2vecModel.most_similar(positive=[word], topn = countSemantic):
					result.append(i[0])	

		documentVector = getVector(result).tolist()
		
		documents.update({'vector': documentVector, 'path': textPath}, documentQuery.hash == documentHash)
		
	currentDocument = documents.search(documentQuery.hash == documentHash)
	
	cosDosuments = []
	
	for document in documents.all():
		cos = getCos(numpy.array(document['vector']), numpy.array(currentDocument[0]['vector']))
		if  math.fabs(cos < 0.3) and math.fabs(cos > 0):
			cosDosuments.append(document['path'])
			
	file.close()
	print(cosDosuments)
	return cosDosuments;


	
