import gensim
import math

modelPath = 'model.bin'

word2vecModel = gensim.models.KeyedVectors.load_word2vec_format(modelPath, binary=True)

word1 = 'мама_NOUN'
word2 = 'танк_NOUN'
	
a = word2vecModel.get_vector(word1)

b = word2vecModel.get_vector(word2)

def getCos(a,b):
	sum = 0;
	sumA = 0;
	sumB = 0;
	i = 0;
	while i < len(a):
		sum = sum + (a[i] * b[i])
		sumA = sumA + (a[i] * a[i])
		sumB = sumB + (b[i] * b[i])
		i = i + 1

	return 1 - (sum / (math.sqrt(sumA) * math.sqrt(sumB)))
	
	
print(getCos(a,b))
print(word2vecModel.distance(word1, word2))