import gensim
modelPath = 'model.bin'
word2vecModel = gensim.models.KeyedVectors.load_word2vec_format(modelPath, binary=True)

words = ['день_NOUN', 'ночь_NOUN', 'человек_NOUN', 'семантика_NOUN', 'студент_NOUN', 'студент_ADJ']

for word in words:
    # есть ли слово в модели? Может быть, и нет
    if word in word2vecModel:
        print(word)
        # выдаем 10 ближайших соседей слова:
        for i in word2vecModel.most_similar(positive=[word], topn=10):
            # слово + коэффициент косинусной близости
            print(i[0], i[1])
        print('\n')
    else:
        # Увы!
        print(word + ' is not present in the model')