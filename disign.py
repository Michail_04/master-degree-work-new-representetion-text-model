# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'disign.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(300, 300)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.documents = QtWidgets.QListWidget(self.centralwidget)
        self.documents.setObjectName("documents")
        self.documents.resize(300, 200)
        self.getDocumentBtn = QtWidgets.QPushButton(self.centralwidget)
        self.getDocumentBtn.setGeometry(QtCore.QRect(10, 220, 281, 41))
        self.getDocumentBtn.setObjectName("getDocumentBtn")
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setGeometry(QtCore.QRect(10, 270, 281, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.getDocumentBtn.setText(_translate("MainWindow", "Выбрать документ"))

